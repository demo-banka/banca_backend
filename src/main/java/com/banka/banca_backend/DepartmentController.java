package com.banka.banca_backend;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DepartmentController {

    @RequestMapping(method = RequestMethod.GET, value = "/")
    public String index() {
        System.out.println("you are on my backend!");
        return "**DEPARTMENT CONTROLLER**";
    }

}
